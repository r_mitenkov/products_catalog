package com.example.validator;

import com.example.dto.SectionDto;
import com.example.model.SectionEntity;
import com.example.repository.SectionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.ValidationException;

@Component
@RequiredArgsConstructor
public class SectionValidator extends BaseValidator<SectionDto, SectionEntity> {

    private final SectionRepository sectionRepository;

    @Override
    public void checkBeforePersist(SectionDto dto) {
        super.checkBeforePersist(dto);

        checkTrue(StringUtils.hasText(dto.getName()), "section name can't be null or empty");
        sectionRepository.findByName(dto.getName()).ifPresent(section -> {
            throw new ValidationException("section name must be unique");
        });

        checkNotNull(dto.getParentSectionId(), "parent section id can't be null");
        if (!dto.getParentSectionId().equals(0L)) {
            checkIfEntityExists(sectionRepository, dto.getParentSectionId(),
                    "section with id " + dto.getParentSectionId() + " not found");
        }
    }

    @Override
    public void checkBeforeUpdate(SectionDto dto) {
        super.checkBeforeUpdate(dto);
        SectionEntity sectionEntity = checkIfEntityExistsAndActual(sectionRepository, dto);

        if (dto.getName() != null) {
            checkTrue(StringUtils.hasText(dto.getName()), "section name can't be empty string");
            sectionRepository.findByName(dto.getName()).ifPresent(section -> {
                throw new ValidationException("section name must be unique");
            });
        }
        if (dto.getParentSectionId() != null && !dto.getParentSectionId().equals(0L)) {
            SectionEntity targetSectionEntity = checkIfEntityExists(sectionRepository, dto.getParentSectionId(),
                    "section with id " + dto.getParentSectionId() + " not found");
            if (dto.getParentSectionId().equals(sectionEntity.getId())) {
                throw new ValidationException("parent section id can't be equals entity id");
            }
            if (firstSectionContainsSecondOrEquals(sectionEntity, targetSectionEntity)) {
                throw new ValidationException("parent section can't be moved into its child");
            }
        }
    }

    private boolean firstSectionContainsSecondOrEquals(SectionEntity firstSection, SectionEntity secondEntity) {
        if (secondEntity.getId().equals(firstSection.getId())) {
            return true;
        }
        if (secondEntity.getParentSection() == null) {
            return false;
        }
        return firstSectionContainsSecondOrEquals(firstSection, secondEntity.getParentSection());
    }

    @Override
    public void checkBeforeDelete(SectionDto dto) {
        super.checkBeforeDelete(dto);
        SectionEntity sectionEntity = checkIfEntityExistsAndActual(sectionRepository, dto);
        if (!sectionEntity.getChildSections().isEmpty() || !sectionEntity.getProducts().isEmpty()) {
            throw new ValidationException("not empty section can't be deleted");
        }
    }
}
