package com.example.validator;

import com.example.dto.ProductDto;
import com.example.model.ProductEntity;
import com.example.repository.ProductRepository;
import com.example.repository.SectionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.validation.ValidationException;

@Component
@RequiredArgsConstructor
public class ProductValidator extends BaseValidator<ProductDto, ProductEntity> {

    private final ProductRepository productRepository;
    private final SectionRepository sectionRepository;

    @Override
    public void checkBeforePersist(ProductDto dto) {
        super.checkBeforePersist(dto);

        checkTrue(StringUtils.hasText(dto.getName()), "product name can't be null or empty");
        productRepository.findByName(dto.getName()).ifPresent(product -> {
            throw new ValidationException("product name must be unique");
        });

        checkNotNull(dto.getSectionId(), "sectionId can't be null");
        checkIfEntityExists(sectionRepository, dto.getSectionId(), "section with id " + dto.getSectionId() + " not found");
    }

    @Override
    public void checkBeforeUpdate(ProductDto dto) {
        super.checkBeforeUpdate(dto);
        ProductEntity productEntity = checkIfEntityExistsAndActual(productRepository, dto);

        if (dto.getName() != null && !productEntity.getName().equals(dto.getName())) {
            checkTrue(StringUtils.hasText(dto.getName()), "product name can't be empty string");
            productRepository.findByName(dto.getName()).ifPresent(product -> {
                throw new ValidationException("product name must be unique");
            });
        }
        if (dto.getSectionId() != null) {
            checkIfEntityExists(sectionRepository, dto.getSectionId(), "section with id " + dto.getSectionId() + " not found");
        }
    }

    @Override
    public void checkBeforeDelete(ProductDto dto) {
        super.checkBeforeDelete(dto);
        checkIfEntityExistsAndActual(productRepository, dto);
    }
}
