package com.example.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@SequenceGenerator(name = "seq_gen", sequenceName = "sections_seq", allocationSize = 1)
@Table(name = "sections", indexes = { @Index(name = "idx_sections_name", columnList = "name") })
@Getter
@Setter
public class SectionEntity extends BaseEntity {

    @Column(name = "name", nullable = false, unique = true, length = 50)
    private String name;

    @OneToMany(mappedBy = "parentSection", fetch = FetchType.LAZY)
    private List<SectionEntity> childSections = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_section_id")
    private SectionEntity parentSection;

    @OneToMany(mappedBy = "section", fetch = FetchType.LAZY)
    private List<ProductEntity> products = new ArrayList<>();
}
