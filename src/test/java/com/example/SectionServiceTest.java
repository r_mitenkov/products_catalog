package com.example;

import com.example.dto.ListingOptions;
import com.example.dto.ProductDto;
import com.example.dto.SectionDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SectionServiceTest extends AbstractTest {

    private static Long rootSectionId;
    private static Long secondSectionId;
    private static Long fifthSectionId;

    private static final String ROOT_SECTION_NAME = "section1";
    private static final String SECTION_2_NAME = "section2";
    private static final String SECTION_3_NAME = "section3";
    private static final String SECTION_4_NAME = "section4";
    private static final String SECTION_5_NAME = "section5";
    private static final String PRODUCT_NAME_1 = "product1";


    @BeforeEach
    public void persistSections() {
        rootSectionId = persistSection(ROOT_SECTION_NAME, 0L).getId();
        secondSectionId = persistSection(SECTION_2_NAME, rootSectionId).getId();
        persistSection(SECTION_3_NAME, secondSectionId);
        Long forthSectionId = persistSection(SECTION_4_NAME, secondSectionId).getId();
        fifthSectionId = persistSection(SECTION_5_NAME, forthSectionId).getId();
    }

    @Test
    public void persistSectionsTest() {
        SectionDto rootSectionDto = sectionService.getSectionWithChildren(rootSectionId, -1L, false)
                .orElseThrow(EntityNotFoundException::new);

        assertEquals(ROOT_SECTION_NAME, rootSectionDto.getName());
        assertEquals(1, rootSectionDto.getChildSections().size());

        SectionDto secondSectionDto = rootSectionDto.getChildSections().get(0);
        assertEquals(SECTION_2_NAME, secondSectionDto.getName());
        assertEquals(rootSectionId, secondSectionDto.getParentSectionId());
        assertEquals(2, secondSectionDto.getChildSections().size());
        assertEquals(SECTION_3_NAME, secondSectionDto.getChildSections().get(0).getName());
        SectionDto forthSectionDto = secondSectionDto.getChildSections().get(1);
        assertEquals(SECTION_4_NAME, forthSectionDto.getName());
        assertEquals(1, forthSectionDto.getChildSections().size());
        assertEquals(SECTION_5_NAME, forthSectionDto.getChildSections().get(0).getName());
    }

    @Test
    public void getSectionsWithChildrenAndProductsTest() {
        SectionDto rootSectionDto = sectionService.getSectionWithChildren(rootSectionId, 0L, false)
                .orElseThrow(EntityNotFoundException::new);
        assertNull(rootSectionDto.getChildSections());

        rootSectionDto = sectionService.getSectionWithChildren(rootSectionId, 1L, false)
                .orElseThrow(EntityNotFoundException::new);
        assertNull(rootSectionDto.getChildSections().get(0).getChildSections());

        persistProduct(PRODUCT_NAME_1, rootSectionDto.getChildSections().get(0).getId());
        rootSectionDto = sectionService.getSectionWithChildren(rootSectionId, 1L, false)
                .orElseThrow(EntityNotFoundException::new);
        assertNull(rootSectionDto.getChildSections().get(0).getProducts());

        rootSectionDto = sectionService.getSectionWithChildren(rootSectionId, 1L, true)
                .orElseThrow(EntityNotFoundException::new);
        List<ProductDto> products = rootSectionDto.getChildSections().get(0).getProducts();
        assertEquals(1, products.size());
        assertEquals(PRODUCT_NAME_1, products.get(0).getName());
    }

    @Test
    public void updateSectionsTest() {
        SectionDto rootSectionDto = sectionService.getSection(rootSectionId, false)
                .orElseThrow(EntityNotFoundException::new);
        SectionDto updatedSectionDto = new SectionDto();
        updatedSectionDto.setId(rootSectionDto.getId());
        updatedSectionDto.setVersion(rootSectionDto.getVersion());
        updatedSectionDto.setName(SECTION_2_NAME);
        assertThrows(ValidationException.class, () -> sectionService.updateSection(updatedSectionDto));

        updatedSectionDto.setName("rootSection");
        sectionService.updateSection(updatedSectionDto);
        rootSectionDto = sectionService.getSectionWithChildren(rootSectionId, 1L, false)
                .orElseThrow(EntityNotFoundException::new);
        assertEquals("rootSection", rootSectionDto.getName());

        updatedSectionDto.setParentSectionId(rootSectionDto.getChildSections().get(0).getId());
        assertThrows(IllegalStateException.class, () -> sectionService.updateSection(updatedSectionDto));
        updatedSectionDto.setVersion(rootSectionDto.getVersion());
        assertThrows(ValidationException.class, () -> sectionService.updateSection(updatedSectionDto));
        updatedSectionDto.setParentSectionId(rootSectionDto.getId());
        assertThrows(ValidationException.class, () -> sectionService.updateSection(updatedSectionDto));

        SectionDto secondSectionDto = sectionService.getSection(secondSectionId, false)
                .orElseThrow(EntityNotFoundException::new);
        updatedSectionDto.setId(secondSectionDto.getId());
        updatedSectionDto.setVersion(secondSectionDto.getVersion());
        updatedSectionDto.setName(null);
        updatedSectionDto.setParentSectionId(fifthSectionId);
        assertThrows(ValidationException.class, () -> sectionService.updateSection(updatedSectionDto));
        updatedSectionDto.setParentSectionId(-1L);
        assertThrows(EntityNotFoundException.class, () -> sectionService.updateSection(updatedSectionDto));
        updatedSectionDto.setParentSectionId(0L);
        sectionService.updateSection(updatedSectionDto);
        assertEquals(2, sectionService.getRootSections(0L, false).size());
    }

    @Test
    public void deleteSectionsTest() {
        SectionDto secondSectionDto = sectionService.getSectionWithChildren(secondSectionId, -1L, false)
                .orElseThrow(EntityNotFoundException::new);
        assertEquals(2, secondSectionDto.getChildSections().size());
        assertThrows(ValidationException.class, () -> sectionService.deleteSection(secondSectionDto));

        SectionDto sectionToDelete = secondSectionDto.getChildSections().get(0);
        sectionService.deleteSection(sectionToDelete);
        assertNull(sectionService.getSection(sectionToDelete.getId(), false).orElse(null));

        sectionToDelete = secondSectionDto.getChildSections().get(1);
        sectionService.deleteSection(sectionToDelete.getChildSections().get(0));
        assertNull(sectionService.getSection(fifthSectionId, false).orElse(null));

        sectionService.deleteSection(sectionToDelete);
        assertNull(sectionService.getSection(sectionToDelete.getId(), false).orElse(null));

        ProductDto productDto = persistProduct(PRODUCT_NAME_1, secondSectionDto.getId());
        assertEquals(1, productService.getProductsBySectionId(new ListingOptions(), secondSectionDto.getId()).getNumberOfElements());
        assertThrows(ValidationException.class, () -> sectionService.deleteSection(secondSectionDto));
        productService.deleteProduct(productDto);
        assertEquals(0, productService.getProductsBySectionId(new ListingOptions(), secondSectionDto.getId()).getNumberOfElements());
        sectionService.deleteSection(secondSectionDto);
        assertNull(sectionService.getSection(secondSectionDto.getId(), false).orElse(null));
        assertEquals(0, productService.getAllProducts(new ListingOptions()).getTotalElements());

        SectionDto rootSectionDto = sectionService.getSectionWithChildren(rootSectionId, 2L, false)
                .orElseThrow(EntityNotFoundException::new);
        assertEquals(0, rootSectionDto.getChildSections().size());
        sectionService.deleteSection(rootSectionDto);
        assertEquals(0, sectionService.getRootSections(0L, false).size());
    }

    @AfterEach
    public void deleteSectionsAndProducts() {
        deleteAllSectionsAndProducts();
    }
}
