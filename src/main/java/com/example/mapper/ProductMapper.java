package com.example.mapper;


import com.example.dto.ProductDto;
import com.example.model.ProductEntity;
import com.example.repository.SectionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;


@Component
@RequiredArgsConstructor
public class ProductMapper {

    private final SectionRepository sectionRepository;

    public ProductDto mapToDto(@NotNull ProductEntity productEntity) {
        ProductDto productDto = new ProductDto();
        productDto.setName(productEntity.getName());
        productDto.setId(productEntity.getId());
        productDto.setSectionId(productEntity.getSection().getId());
        productDto.setVersion(productEntity.getEntityVersion());
        return productDto;
    }

    public ProductEntity mapToNewEntity(@NotNull ProductDto productDto) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setName(productDto.getName());
        sectionRepository.findById(productDto.getSectionId()).ifPresent(productEntity::setSection);
        return productEntity;
    }

    public void mapToExistingEntity(@NotNull ProductDto productDto, @NotNull ProductEntity productEntity) {
        if (productDto.getName() != null) {
            productEntity.setName(productDto.getName());
        }
        if (productDto.getSectionId() != null && !productDto.getSectionId().equals(productEntity.getSection().getId())) {
            sectionRepository.findById(productDto.getSectionId()).ifPresent(productEntity::setSection);
        }
    }
}
