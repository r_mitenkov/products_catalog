package com.example.repository;


import com.example.model.SectionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SectionRepository extends PagingAndSortingRepository<SectionEntity, Long> {

    Optional<SectionEntity> findByName(String name);

    @Query("select s from SectionEntity s where s.parentSection is null")
    List<SectionEntity> findRootSections();
}
