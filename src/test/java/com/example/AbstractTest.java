package com.example;

import com.example.dto.ListingOptions;
import com.example.dto.ProductDto;
import com.example.dto.SectionDto;
import com.example.service.ProductService;
import com.example.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public abstract class AbstractTest {

    @Autowired
    protected SectionService sectionService;
    @Autowired
    protected ProductService productService;

    protected void deleteSection(SectionDto sectionDto) {
        for (SectionDto child : sectionDto.getChildSections()) {
            deleteSection(child);
        }
        sectionService.deleteSection(sectionDto);
    }

    protected SectionDto persistSection(String name, Long parentSectionId) {
        SectionDto sectionDto = new SectionDto();
        sectionDto.setName(name);
        sectionDto.setParentSectionId(parentSectionId);
        return sectionService.persistSection(sectionDto);
    }

    protected ProductDto persistProduct(String name, Long sectionId) {
        ProductDto productDto = new ProductDto();
        productDto.setName(name);
        productDto.setSectionId(sectionId);
        return productService.persistProduct(productDto);
    }

    protected void deleteAllSectionsAndProducts() {
        productService.getAllProducts(new ListingOptions()).forEach(productService::deleteProduct);
        sectionService.getRootSections(0L, false).forEach( rootSection ->
                sectionService.getSectionWithChildren(rootSection.getId(), -1L, false)
                        .ifPresent(this::deleteSection));
        assertEquals(0, sectionService.getRootSections(0L, false).size());
    }
}
