package com.example.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class SectionDto extends BaseDto {

    private String name;
    private Long parentSectionId;
    private List<SectionDto> childSections;
    private List<ProductDto> products;

    public void addProduct(ProductDto product) {
        if (products == null) {
            products = new ArrayList<>();
        }
        products.add(product);
    }
}
