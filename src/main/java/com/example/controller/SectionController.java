package com.example.controller;

import com.example.dto.SectionDto;
import com.example.service.SectionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/sections")
public class SectionController {

    private final SectionService sectionService;

    @PostMapping("/persist")
    ResponseEntity<SectionDto> persistSection(@RequestBody SectionDto sectionDto) {
        return ResponseEntity.ok(sectionService.persistSection(sectionDto));
    }

    @PutMapping("/update")
    ResponseEntity<Void> updateSection(@RequestBody SectionDto sectionDto) {
        sectionService.updateSection(sectionDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/delete")
    ResponseEntity<SectionDto> deleteSection(@RequestBody SectionDto sectionDto) {
        return sectionService.deleteSection(sectionDto).map(ResponseEntity::ok)
                .orElseThrow(EntityNotFoundException::new);
    }

    @GetMapping("/{sectionId}")
    ResponseEntity<SectionDto> getSection(@PathVariable("sectionId") Long sectionId,
                                          @RequestParam(value = "includeProducts", defaultValue = "false") Boolean includeProducts) {
        return sectionService.getSection(sectionId, includeProducts).map(ResponseEntity::ok)
                .orElseThrow(EntityNotFoundException::new);
    }

    @GetMapping("/tree/{rootSectionId}")
    ResponseEntity<SectionDto> getSectionWithChildren(@PathVariable("rootSectionId") Long sectionId,
                                                      @RequestParam(value = "levelsLimit", defaultValue = "0") Long levelsLimit,
                                                      @RequestParam(value = "includeProducts", defaultValue = "false") Boolean includeProducts) {
        return sectionService.getSectionWithChildren(sectionId, levelsLimit, includeProducts).map(ResponseEntity::ok)
                .orElseThrow(EntityNotFoundException::new);
    }

    @GetMapping("/roots")
    ResponseEntity<List<SectionDto>> getSectionWithChildren(@RequestParam(value = "levelsLimit", defaultValue = "0") Long levelsLimit,
                                                            @RequestParam(value = "includeProducts", defaultValue = "false") Boolean includeProducts) {
        return ResponseEntity.ok(sectionService.getRootSections(levelsLimit, includeProducts));
    }
}
