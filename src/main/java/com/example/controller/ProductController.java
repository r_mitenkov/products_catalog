package com.example.controller;

import com.example.dto.ListingOptions;
import com.example.dto.ProductDto;
import com.example.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductController {

    private final ProductService productService;

    @PostMapping("/persist")
    ResponseEntity<ProductDto> persistProduct(@RequestBody ProductDto productDto) {
        return ResponseEntity.ok(productService.persistProduct(productDto));
    }

    @PutMapping("/update")
    ResponseEntity<Void> updateProduct(@RequestBody ProductDto productDto) {
        productService.updateProduct(productDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/delete/{productId}")
    ResponseEntity<ProductDto> deleteProduct(@RequestBody ProductDto productDto) {
        return productService.deleteProduct(productDto).map(ResponseEntity::ok).orElseThrow(EntityNotFoundException::new);
    }

    @GetMapping("/{productId}")
    ResponseEntity<ProductDto> getProduct(@PathVariable("productId") Long productId) {
        return productService.getProduct(productId).map(ResponseEntity::ok).orElseThrow(EntityNotFoundException::new);
    }

    @GetMapping("/by-name/{productName}")
    ResponseEntity<ProductDto> getProduct(@PathVariable("productName") String productName) {
        return productService.getProductByName(productName).map(ResponseEntity::ok).orElseThrow(EntityNotFoundException::new);
    }

    @PostMapping("/list")
    ResponseEntity<Page<ProductDto>> getAllProducts(@RequestBody ListingOptions options) {
        return ResponseEntity.ok(productService.getAllProducts(options));
    }

    @PostMapping("/list/{sectionId}")
    ResponseEntity<Page<ProductDto>> getProductsBySectionId(@RequestBody ListingOptions options,
                                                            @PathVariable("sectionId") Long sectionId) {
        return ResponseEntity.ok(productService.getProductsBySectionId(options, sectionId));
    }
}