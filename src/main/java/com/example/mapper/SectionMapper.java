package com.example.mapper;

import com.example.dto.SectionDto;
import com.example.model.SectionEntity;
import com.example.repository.SectionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

import static java.util.Optional.ofNullable;

@Component
@RequiredArgsConstructor
public class SectionMapper {

    private final SectionRepository sectionRepository;
    private final ProductMapper productMapper;

    public SectionDto mapToDto(@NotNull SectionEntity sectionEntity, boolean includeProducts) {
        return mapToDto(sectionEntity, 0, includeProducts);
    }

    public SectionDto mapToDto(@NotNull SectionEntity sectionEntity, long childLevelsLimit, boolean includeProducts) {
        SectionDto sectionDto = new SectionDto();
        sectionDto.setName(sectionEntity.getName());
        sectionDto.setId(sectionEntity.getId());
        sectionDto.setVersion(sectionEntity.getEntityVersion());
        ofNullable(sectionEntity.getParentSection()).ifPresent(section -> sectionDto.setParentSectionId(section.getId()));
        sectionDto.setChildSections(mapChildSections(sectionEntity, childLevelsLimit, includeProducts));
        if (includeProducts) {
            sectionEntity.getProducts().forEach(product -> sectionDto.addProduct(productMapper.mapToDto(product)));
        }
        return sectionDto;
    }

    private List<SectionDto> mapChildSections(SectionEntity sectionEntity, long childLevelsLimit, boolean includeProducts) {
        if (childLevelsLimit == 0) {
            return null;
        }
        List<SectionDto> sectionDtoList = new ArrayList<>();
        for (SectionEntity child : sectionEntity.getChildSections()) {
            sectionDtoList.add(mapToDto(child, childLevelsLimit - 1, includeProducts));
        }
        return sectionDtoList;
    }

    public SectionEntity mapToNewEntity(@NotNull SectionDto sectionDto) {
        SectionEntity sectionEntity = new SectionEntity();
        sectionEntity.setName(sectionDto.getName());
        if (sectionDto.getParentSectionId() != null) {
            if (sectionDto.getParentSectionId().equals(0L)) {
                sectionEntity.setParentSection(null);
            } else {
                sectionRepository.findById(sectionDto.getParentSectionId()).ifPresent(sectionEntity::setParentSection);
            }
        }
        return sectionEntity;
    }

    public void mapToExistingEntity(@NotNull SectionDto sectionDto, @NotNull SectionEntity sectionEntity) {
        if (sectionDto.getName() != null) {
            sectionEntity.setName(sectionDto.getName());
        }
        if (sectionDto.getParentSectionId() != null) {
            if (sectionDto.getParentSectionId().equals(0L)) {
                sectionEntity.setParentSection(null);
            } else {
                sectionRepository.findById(sectionDto.getParentSectionId()).ifPresent(sectionEntity::setParentSection);
            }
        }
    }
}
