package com.example.service;

import com.example.dto.SectionDto;
import com.example.mapper.SectionMapper;
import com.example.model.SectionEntity;
import com.example.repository.SectionRepository;
import com.example.validator.SectionValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SectionService {

    private final SectionValidator sectionValidator;
    private final SectionMapper sectionMapper;
    private final SectionRepository sectionRepository;

    @Transactional
    public SectionDto persistSection(SectionDto sectionDto) {
        sectionValidator.checkBeforePersist(sectionDto);
        SectionEntity sectionEntity = sectionMapper.mapToNewEntity(sectionDto);
        return sectionMapper.mapToDto(sectionRepository.save(sectionEntity), false);
    }

    @Transactional
    public void updateSection(SectionDto sectionDto) {
        sectionValidator.checkBeforeUpdate(sectionDto);
        sectionRepository.findById(sectionDto.getId())
                .ifPresent(sectionEntity -> sectionMapper.mapToExistingEntity(sectionDto, sectionEntity));
    }

    @Transactional
    public Optional<SectionDto> deleteSection(SectionDto sectionDto) {
        sectionValidator.checkBeforeDelete(sectionDto);
        Optional<SectionEntity> sectionEntity = sectionRepository.findById(sectionDto.getId());
        sectionEntity.ifPresent(sectionRepository::delete);
        return sectionEntity.map(section -> sectionMapper.mapToDto(section, false));
    }

    @Transactional(readOnly = true)
    public Optional<SectionDto> getSection(Long sectionId, Boolean includeProducts) {
        return sectionRepository.findById(sectionId).map(section -> sectionMapper.mapToDto(section, includeProducts));
    }

    @Transactional(readOnly = true)
    public Optional<SectionDto> getSectionWithChildren(Long sectionId, Long childLevelsLimit, Boolean includeProducts) {
        return sectionRepository.findById(sectionId)
                .map(sectionEntity -> sectionMapper.mapToDto(sectionEntity, childLevelsLimit, includeProducts));
    }

    @Transactional(readOnly = true)
    public List<SectionDto> getRootSections(Long childLevelsLimit, Boolean includeProducts) {
        return sectionRepository.findRootSections()
                .stream()
                .map(section -> sectionMapper.mapToDto(section, childLevelsLimit, includeProducts))
                .collect(Collectors.toList());
    }
}
