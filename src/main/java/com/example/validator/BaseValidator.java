package com.example.validator;

import com.example.dto.BaseDto;
import com.example.model.BaseEntity;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

public abstract class BaseValidator<T extends BaseDto, U extends BaseEntity> {

    public void checkBeforePersist(T dto) {
        checkNotNull(dto, "dto must be not null");
        checkNull(dto.getVersion(), "entityVersion must be null for persist operation");
    }

    public void checkBeforeUpdate(T dto) {
        checkNotNull(dto, "dto must be not null");
        checkNotNull(dto.getId(), "entityId must be not null for update operation");
        checkNotNull(dto.getVersion(), "entityVersion must be not null for update operation");
    }

    public void checkBeforeDelete(T dto) {
        checkNotNull(dto, "dto must be not null");
        checkNotNull(dto.getId(), "entityId must be not null for delete operation");
        checkNotNull(dto.getVersion(), "entityVersion must be not null for delete operation");
    }

    protected void checkNotNull(Object object, String errorMessage) {
        if (object == null) {
            throw new ValidationException(errorMessage);
        }
    }

    protected void checkNull(Object object, String errorMessage) {
        if (object != null) {
            throw new ValidationException(errorMessage);
        }
    }

    protected void checkTrue(boolean value, String errorMessage) {
        if (!value) {
            throw new ValidationException(errorMessage);
        }
    }

    protected <K> K checkIfEntityExists(CrudRepository<K, Long> repository, Long entityId, String errorMessage) {
        return repository.findById(entityId).orElseThrow(() -> new EntityNotFoundException(errorMessage));
    }

    protected U checkIfEntityExistsAndActual(CrudRepository<U, Long> repository, T dto) {
        U entity = repository.findById(dto.getId()).orElseThrow(EntityNotFoundException::new);
        if (!entity.getEntityVersion().equals(dto.getVersion())) {
            throw new IllegalStateException("referenceId does not actual");
        }
        return entity;
    }
}
