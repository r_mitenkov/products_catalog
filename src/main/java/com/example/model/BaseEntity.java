package com.example.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@BatchSize(size = 10)
@Getter
@Setter
public abstract class BaseEntity {

    @Id
    @GeneratedValue(generator = "seq_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Long id;

    @Version
    @Column(name = "entity_version", nullable = false)
    private Long entityVersion;

    @Column(name = "entity_created", nullable = false)
    private LocalDateTime created;

    @Column(name = "entity_updated", nullable = false)
    private LocalDateTime lastUpdated;

    @PrePersist
    public void prePersist() {
        created = LocalDateTime.now();
        lastUpdated = created;
    }

    @PreUpdate
    public void preUpdate() {
        lastUpdated = LocalDateTime.now();
    }
}



