package com.example.repository;

import com.example.model.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<ProductEntity, Long> {

    Optional<ProductEntity> findByName(String name);

    @Query("select p from ProductEntity p where p.section.id = :sectionId")
    Page<ProductEntity> findAllBySectionId(@Param("sectionId") Long sectionId, Pageable pageable);
}
