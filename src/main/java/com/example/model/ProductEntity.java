package com.example.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@SequenceGenerator(name = "seq_gen", sequenceName = "products_seq", allocationSize = 1)
@Table(name = "products", indexes = { @Index(name = "idx_products_name", columnList = "name") })
@Getter
@Setter
public class ProductEntity extends BaseEntity {

    @Column(name = "name", nullable = false, unique = true, length = 50)
    private String name;

    @ManyToOne(optional = false)
    @JoinColumn(name = "section_id")
    private SectionEntity section;
}
