package com.example.service;

import com.example.dto.ListingOptions;
import com.example.dto.ProductDto;
import com.example.mapper.ListingOptionsMapper;
import com.example.mapper.ProductMapper;
import com.example.model.ProductEntity;
import com.example.repository.ProductRepository;
import com.example.validator.ProductValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductValidator productValidator;
    private final ProductMapper productMapper;
    private final ListingOptionsMapper listingOptionsMapper;
    private final ProductRepository productRepository;

    @Transactional
    public ProductDto persistProduct(ProductDto productDto) {
        productValidator.checkBeforePersist(productDto);
        ProductEntity productEntity = productMapper.mapToNewEntity(productDto);
        return productMapper.mapToDto(productRepository.save(productEntity));
    }

    @Transactional
    public void updateProduct(ProductDto productDto) {
        productValidator.checkBeforeUpdate(productDto);
        productRepository.findById(productDto.getId())
                .ifPresent(productEntity -> productMapper.mapToExistingEntity(productDto, productEntity));
    }

    @Transactional
    public Optional<ProductDto> deleteProduct(ProductDto productDto) {
        productValidator.checkBeforeDelete(productDto);
        Optional<ProductEntity> productEntity = productRepository.findById(productDto.getId());
        productEntity.ifPresent(productRepository::delete);
        return productEntity.map(productMapper::mapToDto);
    }

    @Transactional(readOnly = true)
    public Optional<ProductDto> getProduct(Long productId) {
        return productRepository.findById(productId).map(productMapper::mapToDto);
    }

    @Transactional(readOnly = true)
    public Optional<ProductDto> getProductByName(String productName) {
        return productRepository.findByName(productName).map(productMapper::mapToDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductDto> getProductsBySectionId(ListingOptions options, Long sectionId) {
        return productRepository.findAllBySectionId(sectionId, listingOptionsMapper.mapToPageable(options))
                .map(productMapper::mapToDto);
    }

    @Transactional(readOnly = true)
    public Page<ProductDto> getAllProducts(ListingOptions options) {
        return productRepository.findAll(listingOptionsMapper.mapToPageable(options))
                .map(productMapper::mapToDto);
    }
}
