package com.example.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ListingOptions {

    private int pageNumber = 0;
    private int pageSize = 20;
    private List<SortedField> sortFields = new ArrayList<>();

    @Getter
    @Setter
    public static class SortedField {
        private String fieldName;
        private Sort.Direction direction = Sort.DEFAULT_DIRECTION;
    }
}
