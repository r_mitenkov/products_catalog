package com.example;

import com.example.dto.ListingOptions;
import com.example.dto.ProductDto;
import com.example.dto.SectionDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProductServiceTest extends AbstractTest {

    private static Long rootSectionId;
    private static Long secondSectionId;

    private static final String ROOT_SECTION_NAME = "section1";
    private static final String SECTION_2_NAME = "section2";
    private static final String SECTION_3_NAME = "section3";
    private static final String SECTION_4_NAME = "section4";

    private static final String PRODUCT_NAME_1 = "product1";
    private static final String PRODUCT_NAME_2 = "product2";
    private static final String PRODUCT_NAME_3 = "product3";

    @BeforeEach
    void persistSectionsAndProducts() {
        rootSectionId = persistSection(ROOT_SECTION_NAME, 0L).getId();
        secondSectionId = persistSection(SECTION_2_NAME, rootSectionId).getId();
        persistSection(SECTION_3_NAME, secondSectionId);
        Long forthSectionId = persistSection(SECTION_4_NAME, secondSectionId).getId();

        persistProduct(PRODUCT_NAME_1, secondSectionId);
        persistProduct(PRODUCT_NAME_2, forthSectionId);
    }

    @Test
    void persistProductsTest() {
        SectionDto secondSectionDto = sectionService.getSectionWithChildren(secondSectionId, -1L, true)
                .orElseThrow(EntityNotFoundException::new);
        SectionDto forthSectionDto = secondSectionDto.getChildSections().get(1);
        assertEquals(SECTION_3_NAME, secondSectionDto.getChildSections().get(0).getName());
        assertEquals(SECTION_4_NAME, forthSectionDto.getName());

        assertEquals(1, secondSectionDto.getProducts().size());
        assertEquals(PRODUCT_NAME_1, secondSectionDto.getProducts().get(0).getName());
        assertEquals(1, forthSectionDto.getProducts().size());
        assertEquals(PRODUCT_NAME_2, forthSectionDto.getProducts().get(0).getName());
    }

    @Test
    void updateProductsTest() {
        ProductDto productDto = sectionService.getSectionWithChildren(secondSectionId, -1L, true)
                .map(sectionDto -> sectionDto.getProducts().get(0))
                .orElseThrow(EntityNotFoundException::new);
        ProductDto updatedProductDto = new ProductDto();
        updatedProductDto.setId(productDto.getId());
        updatedProductDto.setVersion(productDto.getVersion());
        updatedProductDto.setName(PRODUCT_NAME_2);
        assertThrows(ValidationException.class, () -> productService.updateProduct(updatedProductDto));

        updatedProductDto.setName(PRODUCT_NAME_3);
        productService.updateProduct(updatedProductDto);
        productDto = productService.getProduct(productDto.getId()).orElseThrow(EntityNotFoundException::new);
        assertEquals(PRODUCT_NAME_3, productDto.getName());

        updatedProductDto.setSectionId(rootSectionId);
        assertThrows(IllegalStateException.class, () -> productService.updateProduct(updatedProductDto));

        updatedProductDto.setVersion(productDto.getVersion());
        productService.updateProduct(updatedProductDto);
        productDto = productService.getProduct(updatedProductDto.getId()).orElseThrow(EntityNotFoundException::new);
        assertEquals(rootSectionId, productDto.getSectionId());

        updatedProductDto.setVersion(productDto.getVersion());
        updatedProductDto.setSectionId(-1L);
        assertThrows(EntityNotFoundException.class, () -> productService.updateProduct(updatedProductDto));
    }

    @AfterEach
    public void deleteSectionsAndProducts() {
        deleteAllSectionsAndProducts();
        assertEquals(0, productService.getAllProducts(new ListingOptions()).getTotalElements());
    }
}
