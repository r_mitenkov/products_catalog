package com.example.mapper;

import com.example.dto.ListingOptions;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import static org.springframework.data.domain.Sort.Direction.ASC;

@Component
public class ListingOptionsMapper {

    public Pageable mapToPageable(ListingOptions options) {
        Sort sort = null;
        for (ListingOptions.SortedField sortedField : options.getSortFields()) {
            Sort sortField = sortedField.getDirection().equals(ASC) ? Sort.by(sortedField.getFieldName()).ascending()
                    : Sort.by(sortedField.getFieldName()).descending();
            if (sort == null) {
                sort = sortField;
            } else {
                sort.and(sortField);
            }
        }
        return sort == null ? PageRequest.of(options.getPageNumber(), options.getPageSize())
                : PageRequest.of(options.getPageNumber(), options.getPageSize(), sort);
    }
}
